# Image for the Java Bootcamp

# user = abc, password = abc
# sudo: no password

# Build:
# docker build -t java .

# Run:
# docker run --rm --detach --publish 3000:3000 --publish 3001:3001 --env TITLE=Java java:latest
# or, if you want to "mount" a host directory to the /home/abc/PersistentDir directory in the container:
# docker run --rm --detach --publish 3000:3000 --publish 3001:3001 --env TITLE=Java --volume YOUR_CHOICE_OF_PATH:/home/abc/PersistentDir:rw java:latest
# and to use your host user id and host group id (XXXX is given by id -u and YYYY by id -g):
# docker run --rm --detach --publish 3000:3000 --publish 3001:3001 --env TITLE=Java --env PUID=XXXX --env PGID=YYYY --volume YOUR_CHOICE_OF_PATH:/home/abc/PersistentDir:rw java:latest

# Connect to:
# Use a web browser and visit http://127.0.0.1:3000/

FROM	lscr.io/linuxserver/webtop:ubuntu-xfce 

# Give the abc user a regular home and nice prompt
RUN	mkdir /home/abc && mkdir /home/abc/Downloads && mkdir /home/abc/PersistentDir && chown -R abc:abc /home/abc && usermod -d /home/abc abc
RUN	echo "PS1='\u@graoully:\w\$ '" >> /home/abc/.bashrc
RUN 	echo "alias ls='ls --color=auto'" >> /etc/bash.bashrc
ENV	HOME=/home/abc

# Install a few utilities
RUN	apt-get update && apt-get upgrade -y && \
	apt-get install -y zip unzip git gedit tree iputils-ping wget vim net-tools traceroute evince texmaker texlive-fonts-extra

# get rid of gedit warning messages
RUN	echo "#!/bin/bash" > /usr/bin/gedit-null && echo "/usr/bin/gedit \""\$\@\"" 2>/dev/null" >> /usr/bin/gedit-null && chmod 755 /usr/bin/gedit-null
RUN	echo "alias gedit=/usr/bin/gedit-null" >> /etc/bash.bashrc 

# Add bookmarks to Firefox
COPY	profiles.ini /home/abc/.mozilla/firefox/profiles.ini
COPY	prefs.js /home/abc/.mozilla/firefox/sip.default-release/prefs.js
COPY	favicons.sqlite /home/abc/.mozilla/firefox/sip.default-release/favicons.sqlite
COPY	places.sqlite /home/abc/.mozilla/firefox/sip.default-release/places.sqlite    
RUN	chown -R abc:abc /home/abc/.mozilla/ 

# Install Intellij IDEA Community Edition
RUN	wget https://download.jetbrains.com/idea/ideaIC-2023.2.tar.gz && tar -zxvf ideaIC-2023.2.tar.gz -C /opt/
RUN	echo "alias idea='/opt/idea-IC-232.8660.185/bin/idea.sh'" >> /etc/bash.bashrc
# for Application finder to also use the right option:
COPY	jetbrains-idea-ce.desktop /usr/share/applications/ 

# Install a JDK
RUN	apt install -y default-jdk

RUN	echo "export PATH=$PATH" > /etc/environment

# Clean up
RUN	apt autoremove && \
	apt autoclean && \
	apt-get clean && \
	rm -rf \
	/tmp/* \
	/var/lib/apt/lists/* \
	/var/tmp/*

