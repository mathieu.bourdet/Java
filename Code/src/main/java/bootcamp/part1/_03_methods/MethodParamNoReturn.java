package bootcamp.part1._03_methods;

public class MethodParamNoReturn {

	public static void main(String[] args) {
		new MethodParamNoReturn();
	}
	
	MethodParamNoReturn() {
		displayProduct(3, 4);
	}
	
	void displayProduct(int n, int m) {
		int p = n * m;
		System.out.println(n + " * " + m + " = " + p);
	}
}
