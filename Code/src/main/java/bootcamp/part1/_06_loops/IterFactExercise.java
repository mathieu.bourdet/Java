package bootcamp.part1._06_loops;

public class IterFactExercise {

	public static void main(String[] args) {
		new IterFactExercise();
	}
	
	IterFactExercise() {
		System.out.println(factorial(0)); // expected result: 1
		System.out.println(factorial(1)); // expected result: 1
		System.out.println(factorial(5)); // expected result: 120
		System.out.println(factorial(12)); // expected result: 479,001,600
	}

	/* with a foor loop
	int factorial(int n) {
		int f = 1;
		for (int i=1; i <= n; i++) {
			f = f*i;
		}
		return f;
	}*/
	// with a while loop
	int factorial(int n) {
		int f = 1;
		int i = 1;
		while (i <= n ) {
			f = f*i;
			i++;
		}
		return f;
	}

}
