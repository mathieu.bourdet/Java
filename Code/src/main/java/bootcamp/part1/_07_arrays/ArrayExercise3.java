package bootcamp.part1._07_arrays;

import java.util.Arrays;

public class ArrayExercise3 {

	public static void main(String[] args) {
		new ArrayExercise3();
	}

	ArrayExercise3() {
		int[] a = { 1, 2, 3, 4 };
		int[] b = separate(a);
		System.out.println(Arrays.toString(a)); // [1, 2, 3, 4]
		System.out.println(Arrays.toString(b)); // [1, 3, 4, 2] or [3, 1, 4, 2] or ...
		System.out.println(check(a, b)); // true
	}

	int[] separate(int[] t) {
		int[] s = new int[t.length];
		int evenIndex = 0;
		int oddIndex = t.length - 1;
		for (int i = 0; i < t.length; i++) {
			if (t[i] % 2 == 0) {
				s[evenIndex] = t[i];
				evenIndex++;
			} else {
				s[oddIndex] = t[i];
				oddIndex--;
			}
		}
		return s;
	}

	private static boolean check(int[] tabA, int[] tabB) {
		boolean ok = true;
		// check even // odd
		boolean even = true;
		for (int i = 0; i < tabB.length; i++) {
			if (even && (tabB[i] % 2 != 0)) {
				even = false;
			}
			if (!even && tabB[i] % 2 == 0) {
				ok = false;
			}
		}
		// check contain the same numbers
		Arrays.sort(tabA);
		Arrays.sort(tabB);
		for (int i = 0; i < tabA.length; i++) {
			if (tabA[i] != tabB[i]) {
				ok = false;
				break;
			}
		}

		return ok;
	}

}
