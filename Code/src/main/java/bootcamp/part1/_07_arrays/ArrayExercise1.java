package bootcamp.part1._07_arrays;

public class ArrayExercise1 {

	public static void main(String[] args) {
		new ArrayExercise1();
	}

	ArrayExercise1() {
		int[] tab = init(4);
		System.out.println(tab.length); // expected: 4
		System.out.println(tab[0]); // expected: 0
		System.out.println(tab[3]); // expected 3
	}

	int[] init(int s) {
		int[] t = new int[s];
		for (int i = 0; i < s; i++) {
			t[i] = i;
		}
		return t;
	}
}
