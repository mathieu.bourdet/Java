package bootcamp.part1._07_arrays;

import java.util.Arrays;

public class ArrayExercise4 {

	public static void main(String[] args) {
		new ArrayExercise4();
	}

	ArrayExercise4() {
		int[] a = { 1, 2, 3, 4 };
		int[] b = { 1, 2, 3, 4 };
		separateBis(a);
		System.out.println(Arrays.toString(a));
		System.out.println(check(b, a));
	}

	void separateBis(int[] t) {
		int[] s = new int[t.length];
		int evenIndex = 0;
		int oddIndex = t.length - 1;
		for (int i = 0; i < t.length; i++) {
			if (t[i] % 2 == 0) {
				s[evenIndex] = t[i];
				evenIndex++;
			} else {
				s[oddIndex] = t[i];
				oddIndex--;
			}
		}
		// naive, erroneous version: t = s;
		// a = s; // no better: a is not in the scope of separateBis

		// correct version:
		for (int i = 0; i < t.length; i++) {
			t[i] = s[i];
		}

	}

	private static boolean check(int[] tabA, int[] tabB) {
		boolean ok = true;
		// check even // odd
		boolean even = true;
		for (int i = 0; i < tabB.length; i++) {
			if (even && (tabB[i] % 2 != 0)) {
				even = false;
			}
			if (!even && tabB[i] % 2 == 0) {
				ok = false;
			}
		}
		// check contain the same numbers
		Arrays.sort(tabA);
		Arrays.sort(tabB);
		for (int i = 0; i < tabA.length; i++) {
			if (tabA[i] != tabB[i]) {
				ok = false;
				break;
			}
		}

		return ok;
	}

}
