package bootcamp.part1._05_conditional;

public class IfExample {

	public static void main(String[] args) {
		new IfExample();
	}
	
	IfExample() {
		boolean liar = false;
		if (!liar)
			System.out.println("It's true!");
		System.out.println("The End...");
		
		if (!liar)
			System.out.println("It's true!");
			System.out.println("I'm not lying.");
		System.out.println("The End...");
		
		if (!liar) {
			System.out.println("It's true!");
			System.out.println("I'm not lying.");
		}
		System.out.println("The End...");
		
	}
}
