package bootcamp.part1._05_conditional;

public class FactorialExercise {

	public static void main(String[] args) {
		new FactorialExercise();
	}
	
	FactorialExercise() {
		System.out.println(factorial(0)); // expected result: 1
		System.out.println(factorial(1)); // expected result: 1
		System.out.println(factorial(5)); // expected result: 120
		System.out.println(factorial(12)); // expected result: 479,001,600
		System.out.println(factorial(13)); // expected result: 6,227,020,800 // overflow int capacity!
		
		System.out.println(factorial(13L)); // expected result: 6,227,020,800
		System.out.println(factorial(20L)); // expected result: 2,432,902,008,176,640,000
		System.out.println(factorial(21L)); // expected result: 51,090,942,171,709,440,000 // overflow long capacity!

	}
	
	/* bugged version
	int factorial(int n) {
		return n * factorial(n-1);
	}*/
	
	int factorial(int n) {
		try {
			if (n == 0) {
				return 1;
			} else {
				return n * factorial(n - 1);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return 0;
	}

	long factorial(long n) {
		try {
			if (n == 0) {
				return 1;
			} else {
				return n * factorial(n - 1);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return 0;
	}

}
