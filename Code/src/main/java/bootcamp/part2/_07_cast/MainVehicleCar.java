package bootcamp.part2._07_cast;

public class MainVehicleCar {
	public static void main(String[] args) {
		
		Car x = new Car();
		Vehicle y = x;
		Car z = (Car)y;
		
		
		/*
	    Vehicle y = new Vehicle();
	    Car x = (Car)y;
	    Vehicle z = (Vehicle)x;
	    */
		
		/*
	    Vehicle y = new Vehicle();
	    Car x = (Car)y;
	    Vehicle z = x;
	    */
		
		/*
	    Car x = new Car();
	    Vehicle y = x;
	    Car z = y;
	    */
	}
}
