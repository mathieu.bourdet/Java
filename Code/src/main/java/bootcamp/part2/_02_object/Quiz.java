package bootcamp.part2._02_object;

public class Quiz {

	public static void main(String[] args) {
		/*
		Rectangle r1, r2, r3;
		r1 = new Rectangle(4.0, 6.0);
		r2 = r1;
		r3 = r1;
		r3.resize(2.0);
		r1.printDescription();
		r2.printDescription();
		r3.printDescription();
		*/
		
		/*
		Rectangle r1 = new Rectangle(4.0,  6.0);
		System.out.println(r1);
		*/
		
		/*
		Rectangle r1 = new Rectangle(4.0,  6.0);
		Rectangle r2 = r1;
		System.out.println(r2);
		*/
		
		/*
		Rectangle r1 = new Rectangle(4.0,  6.0);
		Rectangle r3 = new Rectangle(4.0,  6.0);
		System.out.println(r3);
		*/
		
		Rectangle r1 = new Rectangle(4.0,  6.0);
		Rectangle r2 = r1;
		Rectangle r3 = new Rectangle(4.0,  6.0);
		System.out.println(r1 == r2);
		System.out.println(r1 == r3);
		
		
	}

}
