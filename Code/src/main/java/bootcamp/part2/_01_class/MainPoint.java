package bootcamp.part2._01_class;

class MainPoint {

	public static void main(String[] args) {
		Point p1;
		p1 = new Point("#1");
		p1.printPosition(); // expected: #1: < -8 ; -2 > (with x and y random in [-10 ; 10[)
		
		Point p2 = new Point(0, 0, "origin");
		p2.printPosition(); // expected: origin: < 0 ; 0 >
		
	}

}

class Point {
	int x;
    int y;
    String label;
    
    Point(String lab) {
        label = lab;
        x = (int)(Math.random()*20 - 10);
        y = (int)(Math.random()*20 - 10);
    }
    
    Point(int x_coordinate, int y_coordinate, String lab) {
        x = x_coordinate;
        y = y_coordinate;
        label = lab;
    }
    
    void printPosition() {
        System.out.println(label + ": < " + x + " ; " + y + " >");
    }
}
