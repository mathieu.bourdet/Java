package bootcamp.part2._03_this;

class Point {
	int x;
    int y;
    String label;
    
    Point(String label) {
        this ( (int)(Math.random()*20 - 10) , (int)(Math.random()*20 - 10), label);
    }
    
    Point(int x, int y, String label) {
        this.x = x;
        this.y = y;
        this.label = label;
    }
    
    void printPosition() {
        System.out.println(label + ": < " + x + " ; " + y + " >");
    }
}
