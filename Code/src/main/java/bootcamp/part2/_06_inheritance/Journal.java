package bootcamp.part2._06_inheritance;

public class Journal extends Document {

	protected int nbArticles;

	public Journal(String title, String publisher, int nbArticles) {
		super(title, publisher);
		this.nbArticles = nbArticles;
	}

	public void getInfo() {
		super.getInfo();
		System.out.print(", nb articles: " + nbArticles);
	}
}
