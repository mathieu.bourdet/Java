package bootcamp.part2._06_inheritance;

public class MainPerson {

	public static void main(String[] args) {
		Person p1, p2, p3;
		Student s1, s2;
		Apprentice a;
		Teacher t;
		p1 = new Person("Patrick");
		p1.enter();
		s1 = new Student("Adrien", 2022);
		s1.enter();
		a = new Apprentice("Pierre", 2023, "IBM");
		a.enter();
		t = new Teacher("David", "English");
		t.enter();
		s2 = a;
		s2.enter();
	}

}
