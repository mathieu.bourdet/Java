package bootcamp.part2._06_inheritance;

public class Dog extends Animal {

	protected int size;
	
	public Dog(int age, int size) {
		setAge(age);
		this.size = size;
	}
	
	public void greet() {
		if (size > 10) {
			System.out.println("WOOOF!");
		} else {
			System.out.println("Waof!");
		}
	}

}
