package bootcamp.part2._06_inheritance;

public class Book extends Document {

	protected String author;
	protected int nbPages;

	public Book(String title, String publisher, String author, int nbPages) {
		super(title, publisher);
		this.author = author;
		this.nbPages = nbPages;
	}

	public void getInfo() {
		super.getInfo();
		System.out.print(", Author: " + author + ", nb Pages: " + nbPages);
	}
	
	@Override
	public String toString() {
		return "This is a Book from " + author;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (nbPages != other.nbPages)
			return false;
		return true;
	}
}
