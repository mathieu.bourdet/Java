package bootcamp.part2._08_abstract;

abstract class Animal {

	protected int age;

	protected int getAge() {
		return age;
	}
	
	abstract String getSound();
	
	protected Animal(int age) {
		this.age = age;
	}
}
