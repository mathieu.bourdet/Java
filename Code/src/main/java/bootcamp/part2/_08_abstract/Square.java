package bootcamp.part2._08_abstract;

public class Square {

	protected double side;
	
	public Square(double side) {
		super();
		this.side = side;
	}
}
